import {
  Body,
  Controller,
  Get,
  Headers,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from './jwt/jwt.guard';
import { JwtService } from '@nestjs/jwt';

@ApiTags('login')
@Controller('login')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtService,
  ) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(JwtGuard)
  checkUser(@Request() req, @Headers('Authorization') token: string) {
    const decodedJwtToken = this.jwtService.decode(
      token.replace('Bearer ', ''),
    );
    console.log(decodedJwtToken);
    return decodedJwtToken;
  }

  @Post()
  async login(@Body() authDto: AuthDto) {
    const userEmail = await this.authService.checkUser(
      authDto.email,
      authDto.password,
    );
    return this.authService.generateToken(userEmail);
  }
}
