import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async checkUser(email, password) {
    const user = await this.userService.findUserByEmail(email);
    if (user) {
      const valid = await this.userService.compareHash(
        password,
        user[0].password,
      );
      if (valid) {
        return user[0];
      } else {
        throw new BadRequestException({ message: 'wrong password' });
      }
    } else {
      throw new BadRequestException({ message: ' user email not found' });
    }
  }

  generateToken(user: any) {
    const dataToken = { id: user.id, email: user.email };
    const token = this.jwtService.sign(dataToken);
    return { token: token };
  }
}
