import { Module } from '@nestjs/common';
import { PartnerController } from './partner.controller';
import { PartnerService } from './partner.service';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from '../transport/kafka.config';
import { PartnerAuthModule } from './partner-auth/partner-auth.module';
import UserRepository from '../user/user.repository';
import { HeaderApiKeyStrategy } from './partner-auth/partner-auth-apikey.strategy';
import { MemberService } from '../member/member.service';
import { CacheModule } from '@nestjs/cache-manager';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [
    ClientsModule.register(kafkaOptions),
    PartnerAuthModule,
    CacheModule.register(),
  ],
  controllers: [PartnerController],
  providers: [
    PartnerService,
    UserRepository,
    HeaderApiKeyStrategy,
    MemberService,
    JwtService,
  ],
})
export class PartnerModule {}
