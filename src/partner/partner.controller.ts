import {
  Body,
  Controller,
  Get,
  HttpRedirectResponse,
  Inject,
  InternalServerErrorException,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ClientKafka,
  EventPattern,
  MessagePattern,
  Payload,
} from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { FlightInquiryDto } from '../dto/flight-inquiry.dto';
import { CACHE_MANAGER, CacheKey } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { PartnerService } from './partner.service';
import { BookingPartnerDto } from '../dto/booking-partner.dto';
import { ApiSecurity, ApiTags } from '@nestjs/swagger';
import { PaymentCustomerDto } from '../dto/payment-customer.dto';
import { MemberService } from '../member/member.service';

@ApiTags('partner')
@ApiSecurity('X-API-KEY', ['X-API-KEY'])
@Controller('partner')
export class PartnerController {
  constructor(
    private readonly partnerService: PartnerService,
    private readonly memberService: MemberService,
    @Inject('KAFKA_SERVICE_INTEGRATION')
    private readonly clientKafka: ClientKafka,
    @Inject(CACHE_MANAGER) private readonly cache: Cache,
  ) {}

  private async sendKafkaRequest(topic: string, payload: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        reject(new Error(`Kafka request to ${topic} timed out`));
      }, 5000);

      this.clientKafka.send(topic, payload).subscribe({
        next: (result) => {
          clearTimeout(timeout);
          resolve(result);
        },
        error: (error) => {
          clearTimeout(timeout);
          reject(error);
        },
      });
    });
  }

  @Get('/inquiry/airports')
  @CacheKey('getAirports')
  @UseGuards(AuthGuard('api-key'))
  async getAirportInfo() {
    try {
      const result = await this.sendKafkaRequest('search-airports', '');
      return result;
    } catch (error) {
      console.error('Error in getAirportInfo:', error);
      throw new InternalServerErrorException(
        'Failed to retrieve airport information',
      );
    }
  }

  @Post('/inquiry/flights')
  @UseGuards(AuthGuard('api-key'))
  async searchFlight(@Body() flightInquiryDto: FlightInquiryDto) {
    try {
      const result = await this.sendKafkaRequest(
        'search-flights',
        JSON.stringify(flightInquiryDto),
      );
      return result;
    } catch (error) {
      console.error('Error in searchFlight:', error);
      throw new InternalServerErrorException('Failed to search for flights');
    }
  }

  @Get('/inquiry/seats')
  @UseGuards(AuthGuard('api-key'))
  async searchSeat(@Query('flightId') flightId: string) {
    try {
      const result = await this.sendKafkaRequest('search-seats', flightId);
      return result;
    } catch (error) {
      console.error('Error in searchSeat:', error);
      throw new InternalServerErrorException('Failed to search for seats');
    }
  }

  @Post('/book/seat')
  @UseGuards(AuthGuard('api-key'))
  async bookSeat(
    @Body() bookingPartnerDto: BookingPartnerDto,
  ): Promise<HttpRedirectResponse> {
    const bookingMessage = await this.partnerService.createBookingMessage(
      bookingPartnerDto,
    );

    const result = this.clientKafka.emit('booking-transaction', {
      key: bookingMessage.transaction_id,
      value: JSON.stringify(bookingMessage),
    });

    const redirect: HttpRedirectResponse = {
      url: `/partner/book/${bookingMessage.transaction_id}`,
      statusCode: 302,
    };
    return redirect;
  }

  @Post('/book/payment')
  @UseGuards(AuthGuard('api-key'))
  async makePayment(@Body() paymentCustomerDto: PaymentCustomerDto) {
    const paymentMessage = await this.memberService.createPaymentMessage(
      paymentCustomerDto,
    );

    const result = this.clientKafka.emit('payment-transaction', {
      key: paymentMessage.transaction_id,
      value: JSON.stringify(paymentMessage),
    });

    const redirect: HttpRedirectResponse = {
      url: `/partner/payment/${paymentMessage.transaction_id}`,
      statusCode: 302,
    };
    return redirect;
  }

  @Get('/redeem')
  async getOnBoard(@Query('reservationCode') reservationCode: string) {
    const boardingMessage = await this.memberService.createBoardingMessage(
      reservationCode,
    );

    const result = this.clientKafka.emit('boarding-transaction', {
      key: boardingMessage.transaction_id,
      value: JSON.stringify(boardingMessage),
    });
    const redirect: HttpRedirectResponse = {
      url: `/partner/redeem/${boardingMessage.transaction_id}`,
      statusCode: 302,
    };
    return redirect;
  }

  @EventPattern('booking-transaction.reply')
  async bookReply(@Payload() data) {
    return await this.cache.set(data.transaction_id, data);
  }

  @EventPattern('payment-transaction.reply')
  async paymentReply(@Payload() data) {
    return await this.cache.set(data.transaction_id, data);
  }

  @EventPattern('boarding-transaction.reply')
  async redeemReply(@Payload() data) {
    return await this.cache.set(data.transaction_id, data);
  }

  @Get('/book/:transactionId')
  @UseGuards(AuthGuard('api-key'))
  async infoTransaction(@Param('transactionId') transactionId: string) {
    return this.cache.get(transactionId);
  }

  @Get('/payment/:transactionId')
  @UseGuards(AuthGuard('api-key'))
  async infoPaymentTransaction(@Param('transactionId') transactionId: string) {
    return this.cache.get(transactionId);
  }

  @Get('/redeem/:transactionId')
  @UseGuards(AuthGuard('api-key'))
  async infoBoardingTransaction(@Param('transactionId') transactionId: string) {
    return this.cache.get(transactionId);
  }

  onModuleInit() {
    this.clientKafka.subscribeToResponseOf('search-airports');
    this.clientKafka.subscribeToResponseOf('search-flights');
    this.clientKafka.subscribeToResponseOf('search-seats');
  }
}
