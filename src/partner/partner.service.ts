import { Injectable, UnauthorizedException } from '@nestjs/common';
import UserRepository from '../user/user.repository';
import { monotonicFactory } from 'ulid';
import { BookingPartnerDto } from '../dto/booking-partner.dto';
import { BookingTransactionDto } from '../dto/booking-transaction.dto';

@Injectable()
export class PartnerService {
  constructor(private readonly userRepository: UserRepository) {}

  async findMemberByEmail(email: string) {
    const foundMember = await this.userRepository.findUserByEmail(email);
    console.log([foundMember]);
    return foundMember[0]['id'];
  }

  private async generateTransactionId() {
    // generate ulid as trx_id
    const ulid = monotonicFactory();
    const transactionId = ulid(Date.now());
    return transactionId;
  }

  async createBookingMessage(bookingPartnerDto: BookingPartnerDto) {
    const userId = await this.findMemberByEmail(
      bookingPartnerDto.reservation.user_email,
    );
    if (!userId) {
      throw new UnauthorizedException('email not defined');
    }

    const transactionId = await this.generateTransactionId();
    const bookingMessage = {
      transaction_id: transactionId,
      reservation: {
        flight_id: bookingPartnerDto.reservation.flight_id,
        transaction_id: transactionId,
        departure_time: bookingPartnerDto.reservation.departure_time,
        seat_id: bookingPartnerDto.reservation.seat_id,
        user_id: userId,
      },
      passenger: {
        first_name: bookingPartnerDto.passenger.first_name,
        last_name: bookingPartnerDto.passenger.last_name,
        email: bookingPartnerDto.passenger.email,
        phone_number: bookingPartnerDto.passenger.phone_number,
        nik: bookingPartnerDto.passenger.nik,
      },
    };

    return bookingMessage;
  }
}
