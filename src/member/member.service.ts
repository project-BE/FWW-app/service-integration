import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { monotonicFactory } from 'ulid';
import { BookingDto } from '../dto/booking.dto';
import { BookingTransactionDto } from '../dto/booking-transaction.dto';
import * as crypto from 'crypto';
import { PaymentCustomerDto } from '../dto/payment-customer.dto';

@Injectable()
export class MemberService {
  constructor(private readonly jwtService: JwtService) {}

  private async generateTransactionId() {
    // generate ulid as trx_id
    const ulid = monotonicFactory();
    const transactionId = ulid(Date.now());
    return transactionId;
  }

  private async generateBookingCode(userId, seatId) {
    const dataToHash = `${userId}-${seatId}-${Date.now()}`;
    const hash = crypto.createHash('sha256').update(dataToHash).digest('hex');
    const bookingCode = hash.substring(0, 8).toUpperCase();
    return bookingCode;
  }

  async createBookingMessage(bookingDto: BookingDto, token: string) {
    const decodedJwtToken = this.jwtService.decode(
      token.replace('Bearer ', ''),
    );
    const userId = decodedJwtToken['id'];
    const transactionId = await this.generateTransactionId();

    const bookingMessage = {
      transaction_id: transactionId,
      reservation: {
        flight_id: bookingDto.reservation.flight_id,
        transaction_id: transactionId,
        departure_time: bookingDto.reservation.departure_time,
        seat_id: bookingDto.reservation.seat_id,
        user_id: userId,
      },
      passenger: {
        first_name: bookingDto.passenger.first_name,
        last_name: bookingDto.passenger.last_name,
        email: bookingDto.passenger.email,
        phone_number: bookingDto.passenger.phone_number,
        nik: bookingDto.passenger.nik,
      },
    };

    return bookingMessage;
  }

  async createPaymentMessage(paymentCustomerDto: PaymentCustomerDto) {
    const transactionId = {
      transaction_id: await this.generateTransactionId(),
    };
    const payment_message = paymentCustomerDto;
    const paymentMessage = { ...transactionId, ...{ payment_message } };
    return paymentMessage;
  }

  async createBoardingMessage(reservationCode: string){
    const boardingMessage = {
      transaction_id: await this.generateTransactionId(),
      reservation_code: reservationCode,
    };
    return boardingMessage;
  }
}
