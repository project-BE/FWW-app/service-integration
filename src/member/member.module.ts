import { Module } from '@nestjs/common';
import { MemberController } from './member.controller';
import { MemberService } from './member.service';
import { kafkaOptions } from '../transport/kafka.config';
import { ClientsModule } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [ClientsModule.register(kafkaOptions), CacheModule.register()],
  controllers: [MemberController],
  providers: [MemberService, JwtService],
  exports: [MemberService],
})
export class MemberModule {}
