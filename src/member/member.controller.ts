import {
  Body,
  Controller,
  Get,
  Headers,
  HttpRedirectResponse,
  Inject,
  InternalServerErrorException,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ClientKafka, EventPattern, Payload } from '@nestjs/microservices';
import { JwtGuard } from '../auth/jwt/jwt.guard';
import { FlightInquiryDto } from '../dto/flight-inquiry.dto';
import { BookingDto } from '../dto/booking.dto';
import { MemberService } from './member.service';
import { CACHE_MANAGER, CacheKey } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { ApiTags } from '@nestjs/swagger';
import { PaymentCustomerDto } from '../dto/payment-customer.dto';

@ApiTags('member')
@Controller('member')
export class MemberController {
  constructor(
    @Inject('KAFKA_SERVICE_INTEGRATION')
    private readonly clientKafka: ClientKafka,
    private readonly memberService: MemberService,
    @Inject(CACHE_MANAGER) private readonly cache: Cache,
  ) {}

  private async sendKafkaRequest(topic: string, payload: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const timeout = setTimeout(() => {
        reject(new Error(`Kafka request to ${topic} timed out`));
      }, 5000);

      this.clientKafka.send(topic, payload).subscribe({
        next: (result) => {
          clearTimeout(timeout);
          resolve(result);
        },
        error: (error) => {
          clearTimeout(timeout);
          reject(error);
        },
      });
    });
  }

  @Get('/inquiry/airports')
  @UseGuards(JwtGuard)
  @CacheKey('getAirports')
  async getAirportInfo() {
    try {
      const result = await this.sendKafkaRequest('search-airports', '');
      return result;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  @Post('/inquiry/flights')
  @UseGuards(JwtGuard)
  async searchFlight(@Body() flightInquiryDto: FlightInquiryDto) {
    try {
      const result = await this.sendKafkaRequest(
        'search-flights',
        JSON.stringify(flightInquiryDto),
      );
      return result;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  @Get('/inquiry/seats')
  @UseGuards(JwtGuard)
  async searchSeat(@Query('flightId') flightId: string) {
    try {
      const result = await this.sendKafkaRequest('search-seats', flightId);
      return result;
    } catch (error) {
      throw new InternalServerErrorException(error.message);
    }
  }

  @Post('/book/seat')
  @UseGuards(JwtGuard)
  async bookSeat(
    @Body() bookingDto: BookingDto,
    @Headers('Authorization') token: string,
  ): Promise<HttpRedirectResponse> {
    const bookingMessage = await this.memberService.createBookingMessage(
      bookingDto,
      token,
    );

    const result = this.clientKafka.emit('booking-transaction', {
      key: bookingMessage.transaction_id,
      value: JSON.stringify(bookingMessage),
    });

    const redirect: HttpRedirectResponse = {
      url: `/member/book/${bookingMessage.transaction_id}`,
      statusCode: 302,
    };
    return redirect;
  }

  @Post('/book/payment')
  @UseGuards(JwtGuard)
  async makePayment(@Body() paymentCustomerDto: PaymentCustomerDto) {
    const paymentMessage = await this.memberService.createPaymentMessage(
      paymentCustomerDto,
    );

    const result = this.clientKafka.emit('payment-transaction', {
      key: paymentMessage.transaction_id,
      value: JSON.stringify(paymentMessage),
    });

    const redirect: HttpRedirectResponse = {
      url: `/member/payment/${paymentMessage.transaction_id}`,
      statusCode: 302,
    };
    return redirect;
  }

  @EventPattern('booking-transaction.reply')
  async bookReply(@Payload() data) {
    return await this.cache.set(data.transaction_id, data);
  }

  @EventPattern('payment-transaction.reply')
  async paymentReply(@Payload() data) {
    return await this.cache.set(data.transaction_id, data);
  }

  @Get('/book/:transactionId')
  @UseGuards(JwtGuard)
  async infoTransaction(@Param('transactionId') transactionId: string) {
    return this.cache.get(transactionId);
  }

  @Get('/payment/:transactionId')
  @UseGuards(JwtGuard)
  async infoPaymentTransaction(@Param('transactionId') transactionId: string) {
    return this.cache.get(transactionId);
  }

  onModuleInit() {
    this.clientKafka.subscribeToResponseOf('search-airports');
    this.clientKafka.subscribeToResponseOf('search-flights');
    this.clientKafka.subscribeToResponseOf('search-seats');
  }
}
