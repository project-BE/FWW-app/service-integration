import { Injectable, InternalServerErrorException } from '@nestjs/common';
import DatabaseService from '../database/database.service';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
class UserRepository {
  constructor(private readonly databaseServices: DatabaseService) {}

  async create(createUserDto: CreateUserDto) {
    try {
      const dbResponse = await this.databaseServices.runQuery(
        `
          INSERT INTO "user" (
            username,
            email,
            password,
            user_type
          ) VALUES (
            $1,
            $2,
            $3,
            $4
          )
          `,
        [
          createUserDto.username,
          createUserDto.email,
          createUserDto.password,
          createUserDto.user_type,
        ],
      );
      return dbResponse;
    } catch (error) {
      throw new InternalServerErrorException('error execute query');
    }
  }

  async findUserByEmail(email: string) {
    try {
      const dbResponse = await this.databaseServices.runQuery(
        `
          SELECT id, email, password from "user" 
          WHERE email = $1 
        `,
        [email],
      );
      return dbResponse.rows;
    } catch (error) {
      throw new InternalServerErrorException('error execute query');
    }
  }
}

export default UserRepository;
