import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
import UserRepository from './user.repository';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async create(createUserDto: CreateUserDto) {
    createUserDto.password = await this.hash(createUserDto.password);
    return this.userRepository.create(createUserDto);
  }

  async findUserByEmail(email: string) {
    const foundUser = await this.userRepository.findUserByEmail(email);
    return foundUser;
  }

  async hash(plainPassword) {
    const saltRounds = 10;
    const hash = await bcrypt.hash(plainPassword, saltRounds);
    return hash;
  }

  async compareHash(plainPassword: string, hash: string) {
    const valid = await bcrypt.compare(plainPassword, hash);
    return valid;
  }
}
