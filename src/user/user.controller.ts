import { Controller, Post, Body, HttpStatus, Res } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('signup')
@Controller('signup')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  async create(
    @Body() createUserDto: CreateUserDto,
    @Res() response: Response,
  ) {
    const check = await this.userService.findUserByEmail(createUserDto.email);
    if (check.length != 0) {
      return response.status(HttpStatus.BAD_REQUEST).send({
        status: HttpStatus.BAD_REQUEST,
        message: 'user already exist',
      });
    }
    await this.userService.create(createUserDto);

    return response.status(HttpStatus.CREATED).send({
      status: HttpStatus.CREATED,
      message: 'user created',
    });
  }
}
