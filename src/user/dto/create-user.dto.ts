import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { optional } from 'joi';
import { USER_TYPE } from '../../enums/collection.enum';

export class CreateUserDto {
  @IsOptional()
  id?: number;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(200)
  username: string;

  @ApiProperty({ required: true })
  @IsEmail()
  email: string;

  @ApiProperty({ required: true })
  @IsString()
  @MinLength(8)
  @MaxLength(32)
  password: string;

  @ApiProperty({ required: true })
  @IsString()
  @MinLength(3)
  @MaxLength(7)
  @IsEnum(USER_TYPE)
  user_type: USER_TYPE;
}
