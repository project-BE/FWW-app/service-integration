import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from './user/user.module';
import * as Joi from 'joi';
import { AuthModule } from './auth/auth.module';
import DatabaseModule from './database/database.module';
import { HttpModule } from '@nestjs/axios';
import { ClientsModule } from '@nestjs/microservices';
import { kafkaOptions } from './transport/kafka.config';
import { PartnerModule } from './partner/partner.module';
import { MemberModule } from './member/member.module';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [
    DatabaseModule.forRootAsync({
      imports: [
        ConfigModule.forRoot({ isGlobal: true, envFilePath: `.env` }),
        CacheModule.register({ ttl: 5 * 60, isGlobal: true }),
      ],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        host: configService.get('POSTGRES_HOST'),
        port: configService.get('POSTGRES_PORT'),
        user: configService.get('POSTGRES_APP_USER'),
        password: configService.get('POSTGRES_PASSWORD'),
        database: configService.get('POSTGRES_DB'),
      }),
    }),
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        POSTGRES_HOST: Joi.string().required(),
        POSTGRES_PORT: Joi.number().required(),
        POSTGRES_APP_USER: Joi.string().required(),
        POSTGRES_PASSWORD: Joi.string().required(),
        POSTGRES_DATABASE: Joi.string().required(),
      }),
    }),
    ClientsModule.register(kafkaOptions),
    HttpModule,
    UserModule,
    AuthModule,
    PartnerModule,
    MemberModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
