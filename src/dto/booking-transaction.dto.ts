import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsDate, IsNumber, IsString, ValidateNested } from 'class-validator';

export class BookingTransactionDto {
  @ApiProperty()
  @IsString()
  transaction_id: string;

  @ApiProperty()
  @ValidateNested()
  @Type(() => Reservation)
  reservation: Reservation;

  @ApiProperty()
  @ValidateNested()
  @Type(() => Passenger)
  passenger: Passenger;
}

class Reservation {
  @ApiProperty()
  @IsNumber()
  flight_id: number;

  @ApiProperty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  departure_time: Date;

  @ApiProperty()
  @IsNumber()
  seat_id: number;

  @ApiProperty()
  @IsNumber()
  user_id: number;
}

class Passenger {
  @ApiProperty()
  @IsString()
  first_name: string;

  @ApiProperty()
  @IsString()
  last_name: string;

  @ApiProperty()
  @IsString()
  email: string;

  @ApiProperty()
  @IsString()
  phone_number: string;

  @ApiProperty()
  @IsString()
  nik: string;
}
