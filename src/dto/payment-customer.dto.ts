import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class PaymentCustomerDto {
  @ApiProperty()
  @IsString()
  booking_code: string;

  @ApiProperty()
  @IsString()
  payment_token_code: string;

  @ApiProperty()
  @IsString()
  transaction_madtrins_id: string;

  @ApiProperty()
  @IsString()
  payment_method_id: string;

  @ApiProperty()
  @IsString()
  payment_method_name: string;
}
