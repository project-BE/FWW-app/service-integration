import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsDate, IsDateString, IsString } from 'class-validator';

export class FlightInquiryDto {
  @ApiProperty()
  @IsString()
  departure_airport?: string;

  @ApiProperty()
  @IsString()
  destination_airport?: string;

  @ApiProperty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  departure_time?: Date;
}
