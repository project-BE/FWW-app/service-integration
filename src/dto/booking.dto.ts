import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsDate, IsNumber, IsString, ValidateNested } from 'class-validator';

class Reservation {
  @ApiProperty()
  @IsNumber()
  flight_id: number;

  @ApiProperty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  departure_time: Date;

  @ApiProperty()
  @IsNumber()
  seat_id: number;
}

class Passenger {
  @ApiProperty()
  @IsString()
  first_name: string;

  @ApiProperty()
  @IsString()
  last_name: string;

  @ApiProperty()
  @IsString()
  email: string;

  @ApiProperty()
  @IsString()
  phone_number: string;

  @ApiProperty()
  @IsString()
  nik: string;
}

export class BookingDto {
  @ApiProperty()
  @ValidateNested()
  @Type(() => Reservation)
  reservation: Reservation;

  @ApiProperty()
  @ValidateNested()
  @Type(() => Passenger)
  passenger: Passenger;
}
