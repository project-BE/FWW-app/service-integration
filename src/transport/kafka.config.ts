import { ConfigService } from '@nestjs/config';
import { ClientsModuleOptions, Transport } from '@nestjs/microservices';

const configService = new ConfigService();

export const kafkaOptions: ClientsModuleOptions = [
  {
    name: configService.get('KAFKA_TOKEN'),
    transport: Transport.KAFKA,
    options: {
      client: {
        clientId: configService.get('KAFKA_CLIENT_ID'),
        brokers: [configService.get('KAFKA_BROKERS')],
      },
      consumer: {
        groupId: configService.get('KAFKA_GROUPID'),
      },
    },
  },
];
